<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//   return $request->user();
//});

Route::post('/apilogin', 'ApiController@login')->middleware('cors');
Route::post('/apisignup', 'ApiController@Signup')->middleware('cors');
Route::group([
    'middleware' => 'auth:api'
], function() {
    Route::get('/apidetails', 'ApiController@details')->middleware('cors');
    Route::get('/apilogout', 'ApiController@logout')->middleware('cors');
});


